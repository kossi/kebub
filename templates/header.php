<!DOCTYPE html>
<html lang="fi">
<head>
  <meta charset="UTF-8">
  <title>Kebub - <?php echo strip_tags($allsites[$site]); ?></title>
  <link href="styles/bootstrap.min.css" rel="stylesheet">
  <link href="styles/bootstrap-responsive.min.css" rel="stylesheet">
  <link href="styles/style.css" rel="stylesheet">
</head>
<body>
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container">
        <a class="brand" href=".">Kebub</a>
        <div class="nav-collapse">
          <ul class="nav">
            <?php
              foreach($sites as $k => $v){
                if($k == $site)
                 echo("<li class=\"active\"> <a href=\"?l={$k}\">{$v}</a></li>");
                else 
                  echo("<li> <a href=\"?l={$k}\">{$v}</a></li>");
              }
            ?>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="content">
        <div class="page-header">
          <h1><?php echo strip_tags($allsites[$site]); ?></h1>
        </div>
      
