<?php
class Review{
  private $id;
  private $name;
  private $email;
  private $phone;
  private $freeTxt;
  private $qpRatio;
  private $rating;
  private $beer;

  public static function getStars(){
    return 5;
  }

  public static function getGPRatios(){
    return array(
      5 => "Erinomainen",
      4 => "Hyvä",
      3 => "Keskiverto",
      2 => "Heikko",
      1 => "Ryöstö",
      0 => ""
    );
  }

  public static function getBeers(){
    return array(
      4 => "Olutanniskelun",
      3 => "Kotiinkuljetuksen",
      2 => "Lounaan",
      1 => "Maksun luottokortilla"
    );
  }

  function __construct($name = "", $email = "", $phone = "", $freeTxt = "", $qpRatio = 0,
      $rating = 0, $beer = Array()){
    $this->id = 0;
    $this->name = $name;
    $this->email = $email;
    $this->phone = $phone;
    $this->freeTxt = $freeTxt;
    $this->qpRatio = $qpRatio;
    $this->rating = $rating;
    $this->beer = $beer;
  }

  public function __get($property){ 
    return $this->$property; 
  } 
  
  public function __set($property, $value) {
    if(method_exists($this, 'check' . ucfirst($property))){
      if(call_user_func(array($this, "check" . ucfirst($property)), $value))
        $this->$property = $value;
      else{
        $this->$property = "";
        throw new Exception($property." wrong value");
      }
    }
  }
  public function __toString(){
    return "ID: ".$this->id." Name: ".$this->name;
  }
  public function persist(){
    $rbean = R::dispense("review");
    if($this->id == 0){
      $rbean->name = $this->name;
      $rbean->email = $this->email;
      $rbean->phone = $this->phone;
      $rbean->freetxt = $this->freeTxt;
      $rbean->ratio = $this->qpRatio;
      $rbean->rating = $this->rating; 
      $rbean->beer = implode(",", $this->beer);
      return R::store($rbean);
    }
    else{
      $rbean = R::load("review", $this->id);
      $rbean->name = $this->name;
      $rbean->email = $this->email;
      $rbean->phone = $this->phone;
      $rbean->freetxt = $this->freeTxt;
      $rbean->ratio = $this->qpRatio;
      $rbean->rating = $this->rating; 
      $rbean->beer = implode(",", $this->beer);
      return R::store($rbean);
    }
  }

  public static function fromBean($bean){
    $instance = new self();
    $instance->id = $bean->id;
    $instance->name = $bean->name;
    $instance->email = $bean->email;
    $instance->phone = $bean->phone;
    $instance->freeTxt = $bean->freetxt;
    $instance->qpRatio = $bean->ratio;
    $instance->rating = $bean->rating;
    $instance->beer = explode(",", $bean->beer);
    return $instance;
  }

  private function checkName($str){
    $length = strlen($str);
    if(($length > 2 && $length < 50) && 
        preg_match("/^[\sA-Za-zÄÖÅäöå!?'´,0-9_]+$/", $str) &&
        R::findOne('review',' id != :id AND name = :name', array(':id'=>$this->id, ':name'=>$str)) == null)
      return true;
    else 
      return false;
  }

  private function checkEmail($str){
    if (filter_var($str, FILTER_VALIDATE_EMAIL) &&
        R::findOne('review',' id != :id AND email = :email', array(':id'=>$this->id, ':email'=>$str)) == null)
      return true;
    else 
      return false;
  }

  private function checkPhone($str){
    $length = strlen($str);
    if(($length > 3 && $length < 16) &&
        preg_match("/^[\s0-9_]+$/", $str))
      return true;
    else 
      return false;
  }

  private function checkFreeTxt($str){
    if(empty($str) || (strlen($str) <= 300 && 
        preg_match("/^[\sA-Za-zÄÖÅäöå!?:;'´,0-9_]+$/", $str)))
      return true;
    else 
      return false;
  }

  private function checkRating($str){
    if(is_numeric($str) && $str > 0 && $str <= Review::getStars())
      return true;
    else 
      return false;
  }

  private function checkQpRatio($str){
    if(array_key_exists($str, Review::getGPRatios()) && $str != 0)
      return true;
    else 
      return false;
  }
  private function checkBeer($keys){
    if(is_array($keys)){
      foreach($keys as $key){
        if(array_key_exists($key, Review::getBeers()))
          return true;
      }
    }
    return false;
  }
}
?>