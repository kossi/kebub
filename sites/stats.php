<div class="row">
  <div class="span4 offset2">
    <table class="table table-striped table-bordered table-condensed">
      <thead>
        <tr>
          <th>Sivu</th>
          <th style="text-align: center;">Käyntikerrat</th>
        </tr>
      </thead>
      <tbody>
        <?php
        foreach($sites as $skey => $title){
        ?>
        <tr>
          <td><?php echo strip_tags($title) ?></td>
          <td style="text-align: center;">
          <?php 
            if(isset($_COOKIE[$skey]))
              echo htmlspecialchars($_COOKIE[$skey]);
            else
              echo 0
          ?>
          </td>
        </tr>
        <?php }?>
      </tbody>
    </table>
  </div>
</div>