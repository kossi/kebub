<?php

session_start();
  
if(isset($_SESSION['review'])){
  $review = $_SESSION['review'];
  if(isset($_GET["a"])){
    $action = $_GET["a"];
    if($action == "save"){
      $review->persist();
      unset($_SESSION['review']);
      $url = "http://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"]."?l=saved";
      header("Location: ".$url);
      exit;
    }
    else if($action == "cancel"){
      unset($_SESSION['review']);
      $url = "http://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"];
      header("Location: ".$url);
      exit;
    }
  }
}
else if(empty($_SESSION["review"]) && isset($_GET["id"])){
  $r = R::load('review', $_GET["id"]);
  $review = Review::fromBean($r);
  $_SESSION['review'] = $review;
}
else{
  $url = "http://".$_SERVER["SERVER_NAME"];
  header("Location: ".$url);
  exit;
}


?>
<div class="row">
  <div class="span3">
    <h3>Nimi: </h3>
    <div class="well">
      <?php echo(htmlspecialchars($review->name)); ?>
    </div>
  </div>
  <div class="span2 offset1">
    <h3>Puhelinnumero: </h3>
    <div class="well">
      <?php echo(htmlspecialchars($review->phone)); ?>
    </div>
  </div>
</div>
<div class="row">
  <div class="span3">
    <h3>Sähköpostiosoite: </h3>
    <div class="well">
      <?php echo(htmlspecialchars($review->email)); ?>
    </div>
  </div>
  <div class="span2 offset1">
    <h3>Hintalaatusuhde: </h3>
    <div class="well">
      <?php $ratios = Review::getGPRatios(); echo($ratios[$review->qpRatio]); ?>
    </div>
  </div>
</div>
<div class="row">
  <div class="span3">
    <h3>Lisäpalvelut: </h3>
      <ul style="list-style:none; margin: 0px 5px 10px;">
      <?php
        foreach($review->beer as $beer){
          $beers = Review::getBeers(); echo "<li><i class=\"icon-ok\"></i> ".$beers[$beer]."</li>";
        }
      ?>
      </ul>
  </div>
  <div class="span2 offset1">
    <h3>Tähtien määrä: </h3>
    <span> <?php echo str_repeat("<i class=\"icon-star\"></i>", $review->rating);
    echo str_repeat("<i class=\"icon-star-empty\"></i>", Review::getStars() - $review->rating)?></span>
  </div>
  
</div>
<div class="row">
  <div class="span4">
    <h3>Vapaa sana ravintolasta: </h3>
    <div class="well" style="min-height: 82px;">
      <?php echo(htmlspecialchars($review->freeTxt)); ?>
    </div>
  </div>
</div>
<hr>
<div class="row">
  <div class="span4">
    <a class="btn btn-large" href="?l=add">Korjaa</a>
    
  </div>
  <div class="span2 offset2">
    <a class="btn btn-danger btn-large" href="?l=save&amp;a=cancel">Peruuta</a>
    <a class="btn btn-primary btn-large" href="?l=save&amp;a=save">Tallenna</a>  
  </div>
</div>