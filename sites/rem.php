<?php
if(isset($_GET["i"]) && is_numeric($_GET["i"])){
  $review = R::load('review', $_GET["i"]);
  if(!$review->id){
    $url = "http://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"]."?l=rem";
    header("Location: ".$url);
    exit;
  }
  else{
    R::trash($review);
    $url = "http://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"]."?l=rem";
    header("Location: ".$url);
    exit;
  }
}
?>

<div class="row">
  <div class="span8">
  <table class="table table-striped">
        <thead>
          <tr>
            <th>Ravintolan nimi</th>
            <th>Sähköpostiosoite</th>
            <th style="text-align: center;">Tähdet</th>
            <th style="text-align: center;">Poista</th>
            <th style="text-align: center;">Info</td>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach(R::find('review') as $review){
          ?>
          <tr>
            <td><?php echo htmlspecialchars($review->name); ?></td>
            <td><?php echo htmlspecialchars($review->email); ?></td>
            <td style="text-align: center;"><?php 
            echo str_repeat("<i class=\"icon-star\"></i>", $review->rating);
            echo str_repeat("<i class=\"icon-star-empty\"></i>", Review::getStars() - $review->rating);?></td>
            <td style="text-align: center;"><a class="btn btn-small btn-danger" 
            href="<?php echo "http://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"]."?l=rem&i=".$review->id; ?>"><i class="icon-trash icon-white"></i></a></td>
            <td style="text-align: center;"><a class="btn btn-small btn-info" 
            href="<?php echo "http://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"]."?l=save&id=".$review->id; ?>">
            <i class="icon-info-sign icon-white"></i></a></td>
          </tr>
          <?php }?>
        </tbody>
      </table>
  </div>
</div>