<?php
$ratios = Review::getGPRatios();
?>
<div class="row">
  <div class="span8">
    <table class="table table-striped table-bordered table-condensed">
        <thead>
          <tr>
            <th>#</th>
            <th>Ravintolan nimi</th>
            <th>Sähköpostiosoite</th>
            <th>Numero</th>
            <th>Hintalaatusuhde</th>
            <th style="text-align: center;">Tähdet</th>
          </tr>
        </thead>
        <tbody>
          <?php
          foreach(R::find('review') as $review){
          ?>
          <tr>
            <td><a href="<?php echo "http://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"]."?l=save&id=".$review->id; ?>">
            <?php echo htmlspecialchars($review->id);?></a>
            <td><?php echo htmlspecialchars($review->name); ?></td>
            <td><?php echo htmlspecialchars($review->email); ?></td>
            <td><?php echo htmlspecialchars($review->phone); ?></td>
            <td><?php echo $ratios[$review->ratio];?></td>
            <td style="text-align: center;"><?php 
            echo str_repeat("<i class=\"icon-star\"></i>", $review->rating);
            echo str_repeat("<i class=\"icon-star-empty\"></i>", Review::getStars() - $review->rating);?></td>
          </tr>
          <?php }?>
        </tbody>
      </table>
  </div>
</div>