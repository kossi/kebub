<?php

session_start();
$review = new Review;

if(isset($_POST["reset"])){
  unset($_SESSION['review']);
  $url = "http://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"];
  header("Location: ".$url);
  exit;
}

if(isset($_SESSION["review"]))
  $review = $_SESSION["review"];

$exceptions = array();

try{
  if(isset($_POST["save"]) && isset($_POST["inputBeer"]) && isset($_POST["inputRating"])){
    $review->id = is_numeric($_POST["inputID"]) ? $_POST["inputID"] : 0;
    $review->name = $_POST["inputName"];
    $review->email = $_POST["inputEmail"];
    $review->phone = $_POST["inputPhone"];
    $review->freeTxt = $_POST["inputFreetxt"];
    $review->qpRatio = $_POST["inputQPratio"];
    $review->rating = $_POST["inputRating"];
    $review->beer = $_POST["inputBeer"];
    $url = "http://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"]."?l=save";
    $_SESSION['review'] = $review;
    header("Location: ".$url);
    exit;
             
  }
}
catch(Exception $e){
  unset($_SESSION['review']);
}

?>

<div class="row">
  <div class="span8">
    <fieldset>
      <form action="?l=add" method="POST">
      <input type="hidden" id="inputID" name="inputID" value="<?php echo htmlspecialchars($review->id) ?>">
        <?php 
        try{
          if(isset($_SESSION["review"]))
            $name = $review->name;
          if(isset($_POST["save"])){
            $name = $_POST["inputName"];
            $review->name = $name;
          }
        ?>
        <div class="control-group">
          <label class="control-label" for="inputName">Ravintolan nimi</label>
          <div class="controls">
            <input class="input-xlarge" id="inputName" name="inputName" size="30" type="text" 
            value="<?php if(isset($name)) echo htmlspecialchars($name); ?>">
          </div>
        </div>
            <?php } catch (Exception $e){ $exceptions[] = $e;?>
         <div class="control-group error">
          <label class="control-label" for="inputName">Ravintolan nimi</label>
          <div class="controls">
            <input class="input-xlarge" id="inputName" name="inputName" size="30" type="text" 
            value="<?php if(isset($name)) echo htmlspecialchars($name); ?>">
            <span class="help-inline"><i class="icon-exclamation-sign"></i> Min 3 Max 50 ja ainoastaa a-å sekä erikoismerkit(!?'´,). Uniikki!</span>
             </div>
        </div>
        <?php }?>
        <?php 
          try{
          if(isset($_SESSION["review"]))
            $email = $review->email;
          if(isset($_POST["save"])){
            $email = $_POST["inputEmail"];
            $review->email = $email;
          }
        ?>
        <div class="control-group">
          <label class="control-label" for="inputEmail">Sähköpostiosoite</label>
          <div class="controls">
              <input class="input-xlarge" id="inputEmail" name="inputEmail" size="30" type="text"
              value="<?php if(isset($email)) echo htmlspecialchars($email); ?>">
          </div>
        </div>
            <?php } catch (Exception $e){ $exceptions[] = $e;?>
         <div class="control-group error">
          <label class="control-label" for="inputEmail">Sähköpostiosoite</label>
          <div class="controls">
              <input class="input-xlarge" id="inputEmail" name="inputEmail" size="30" type="text"
              value="<?php if(isset($email)) echo htmlspecialchars($email); ?>">
              <span class="help-inline"><i class="icon-exclamation-sign"></i> Virheellinen sähköpostiosoite. Esim: nimi@osoite.fi (ei tue unicode)</span>
          </div>
        </div>
        <?php }?>
        <?php 
          try{
          if(isset($_SESSION["review"]))
            $phone = $review->phone;
          if(isset($_POST["save"])){
            $phone = $_POST["inputPhone"];
            $review->phone = $phone;
          }
        ?>
        <div class="control-group">
          <label class="control-label" for="inputPhone">Puhelinnumero</label>
          <div class="controls">
            <input class="input-medium" id="inputPhone" name="inputPhone" size="30" type="text"
            value="<?php if(isset($phone)) echo htmlspecialchars($phone);?>">
          </div>
        </div>
            <?php } catch (Exception $e){ $exceptions[] = $e; ?>
         <div class="control-group error">
          <label class="control-label" for="inputPhone">Puhelinnumero</label>
          <div class="controls">
            <input class="input-medium" id="inputPhone" name="inputPhone" size="30" type="text"
            value="<?php if(isset($phone)) echo htmlspecialchars($phone);?>">
            <span class="help-inline"><i class="icon-exclamation-sign"></i> Vain numeroita, mutta tyhjät välimerkit voi jättää. Min 4 max 16</span>
             </div>
        </div>
        <?php }?>
        <?php 
          try{
            if(isset($_SESSION["review"]))
              $freeTxt = $review->freeTxt;
          if(isset($_POST["save"])){
            $freeTxt = $_POST["inputFreetxt"];
            $review->freeTxt = $freeTxt;
          }
        ?>
        <div class="control-group">
          <label class="control-label" for="inputFreetxt">Vapaa sana (katuosoite, kommentit jne!)</label>
          <div class="controls">
            <textarea class="input-xlarge" id="inputFreetxt" name="inputFreetxt" rows="4"><?php if(isset($freeTxt)) echo htmlspecialchars($freeTxt);?></textarea>
          </div>
        </div>
            <?php } catch (Exception $e){ $exceptions[] = $e; ?>
        <div class="control-group error">
          <label class="control-label" for="inputFreetxt">Vapaa sana (katuosoite, kommentit jne!)</label>
          <div class="controls">
            <textarea class="input-xlarge" id="inputFreetxt" name="inputFreetxt" rows="4"><?php if(isset($freeTxt)) echo htmlspecialchars($freeTxt);?></textarea>
            <span class="help-inline"><i class="icon-exclamation-sign"></i> Maksimipituus 300 merkkiä. Ei html-tageja tai muita erikoisia merkkejä</span>
          </div>
        </div>
        <?php }
          try{
          if(isset($_POST["save"])){
            if(!isset($_POST["inputQPratio"])) throw new Exception("No QPratio selected");
            $review->qpRatio = $_POST["inputQPratio"];
          }
        ?>
        <div class="control-group">
          <label class="control-label" for="inputQPratio">Ravintolan hintalaatusuhde</label>
          <div class="controls">
            <select class="input-medium" name="inputQPratio" id="inputQPratio">
              <?php
              foreach(Review::getGPRatios() as $qpval => $qpratio){
                echo("<option value=\"{$qpval}\" ");
                if($review->qpRatio == $qpval)
                  echo("selected=\"selected\"");
                echo(">{$qpratio}</option>");
              }
              ?>
            </select>
          </div>
        </div>
        <?php }catch (Exception $e){ $exceptions[] = $e;?>
        <div class="control-group error">
          <label class="control-label" for="inputQPratio">Ravintolan hintalaatusuhde</label>
          <div class="controls">
            <select class="input-medium" name="inputQPratio" id="inputQPratio">
              <?php
              foreach(Review::getGPRatios() as $qpval => $qpratio){
                echo("<option value=\"{$qpval}\" ");
                if(0 == $qpval)
                  echo("selected=\"selected\"");
                echo(">{$qpratio}</option>");
              }
              ?>
            </select>
            <span class="help-inline"><i class="icon-exclamation-sign"></i> Ravintolalle on annettava hintalaatusuhde!</span>
          </div>
        </div>
        <?php }
         try{
          if(isset($_POST["save"])){
            if(!empty($_POST["inputBeer"]))
              $review->beer = $_POST["inputBeer"];
            else
              throw new Exception("No beer selected");
          }
        ?>
        <div class="control-group">
          <label class="control-label" id="inputBeer">Tarjoaako ravintola?</label>
          <div class="controls">
            <?php
            foreach(Review::getBeers() as $beerval => $beer){
            ?>
                <label class="checkbox">
                  <input type="checkbox" name="inputBeer[]" value="<?php echo($beerval); ?>"
                  <?php if(in_array($beerval, $review->beer))
                   echo "checked=\"checked\"";?>>
                  <?php echo($beer);?>
                </label>
            <?php }?>
          </div>
        </div>
        <?php }catch (Exception $e){ $exceptions[] = $e;?>
        <div class="control-group error">
          <label class="control-label" id="inputBeer">Tarjoaako ravintola?</label>
          <span class="help-inline"><i class="icon-exclamation-sign"></i> Kyllä sen ravintolan on jotain tarjottava!</span>
          <div class="controls">
            <?php
            foreach(Review::getBeers() as $beerval => $beer){
            ?>
                <label class="checkbox">
                  <input type="checkbox" name="inputBeer[]" value="<?php echo($beerval); ?>">
                  <?php echo($beer);?>
                </label>
            <?php }?>
          </div>
        </div>
        <?php
        }
          try{
          if(isset($_POST["save"])){
            if(!isset($_POST["inputRating"])) throw new Exception("No rating selected");
            $review->rating = $_POST["inputRating"];
          }
        ?>
        <div class="control-group">
          <label class="control-label" id="inputRating">Montako tähteä antaisit ravintolalle?</label>
          <div class="controls">
              <?php
                foreach(range(Review::getStars(), 1, -1) as $star){
              ?>
                <label class="radio inline">
                  <input type="radio" name="inputRating" value="<?php echo $star;?>"
                  <?php if($review->rating == $star) echo("checked=\"checked\"")?>>
                  <span> <?php echo str_repeat("<i class=\"icon-star\"></i>", $star);?></span>
                 </label>
              <?php }?>
          </div>
        </div>
        <?php } catch (Exception $e){ $exceptions[] = $e; ?>
        <div class="control-group error">
          <label class="control-label" id="inputRating">Montako tähteä antaisit ravintolalle?</label>
          <div class="controls">
              <?php
                foreach(range(Review::getStars(), 1, -1) as $star){
              ?>
                <label class="radio inline">
                  <input type="radio" name="inputRating" value="<?php echo $star;?>">
                  <span> <?php echo str_repeat("<i class=\"icon-star\"></i>", $star);?></span>
                 </label>
              <?php } ?>
              <span class="help-inline"><i class="icon-exclamation-sign"></i> Anna nyt edes vähän tähtiä!</span>
          </div>
        </div>
        <?php }?>
        <br>
        <div class="control-group">
          <button class="btn btn-large btn-primary" type="submit" name="save">Tallenna ravintola</button>
          <button class="btn btn-large" type="submit" name="reset">Peruuta</button>
        </div>
      </form>
    </fieldset>
  </div>
</div>
<?php
  if(isset($_POST["save"]) && empty($exceptions))
    echo "<span>OK!</span>";
  else if(isset($_POST["save"]))
    echo "<span id=\"err\">Virheitä lomakkeessa!</span>";
   // echo var_dump($exceptions);
?>