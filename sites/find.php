<div class="row">
  <div class="span6">
    <form class="form-search" action="?l=find" method="POST">
      <input type="text" name="inputFind" class="input search-query">
      <button type="submit" class="btn btn-primary"><i class="icon-search icon-white"></i> Search</button>
    </form>
  </div>
</div>
<div class="row">
  <div class="span8">
    <table class="table table-striped table-condensed">
        <thead>
          <tr>
            <th>#</th>
            <th>Ravintolan nimi</th>
            <th>Sähköpostiosoite</th>
            <th>Numero</th>
            <th style="text-align: center;">Tähdet</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(isset($_POST["inputFind"])){
            $query = $_POST["inputFind"];
            foreach(R::find('review', ' name LIKE :name', array(':name'=>$query)) as $review){
          ?>
          <tr>
            <td><a href="<?php echo "http://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"]."?l=save&id=".$review->id; ?>">
            <?php echo htmlspecialchars($review->id);?></a>
            <td><?php echo htmlspecialchars($review->name); ?></td>
            <td><?php echo htmlspecialchars($review->email); ?></td>
            <td><?php echo htmlspecialchars($review->phone); ?></td>
            <td style="text-align: center;"><?php 
            echo str_repeat("<i class=\"icon-star\"></i>", $review->rating);
            echo str_repeat("<i class=\"icon-star-empty\"></i>", Review::getStars() - $review->rating);?></td>
          </tr>
          <?php }}?>
        </tbody>
      </table>
  </div>
</div>