<div class="row">
  <div class="span8">
  <h3>Tervetuloa Kebub-sivulle, jossa voit arvostella kebab-ravintoloita!</h3>
  <p>Valitse yläriviltä toiminto, jonka haluat tehdä. Tämän sovelluksen kehittämiseen on käytetty seuraavia ohjelmistoja/kirjastoja: </p>
  <ul>
    <li>Ohjelmointityö tehty <a href="http://www.sublimetext.com/2">Sublime Text 2</a> -ohjelmalla</li>
    <li>CSS-tyylin apuna käytetään <a href="http://twitter.github.com/bootstrap">Bootstrap</a>-tyyliä, joka on tehty Twitterin toimesta</li>
    <li>Kehityksen aikana http-palvelimena toiminut <a href="http://nginx.org/">nginx</a></li>
    <li>Versionhallinta <a href="http://git-scm.com/">Git</a>-ohjelmistolla</li> 
    <li>Itse www-sivun dynaamisuus toteutettu <a href="http://php.net/">PHP</a>-ohjelmointikielellä 5.3.9 versiolla</li>
    <li>Sivun ikoinena käytin <a href="http://glyphicons.com/">GLYPHICONS</a>-ikoneita</li>
    <li>Tietokannan ja PHP:n välillä käytin <a href="http://www.redbeanphp.com/">RedBeanPHP</a>-kirjastoa</li>
  </ul>
  </div>
</div>