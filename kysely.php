<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Kyselymerkkijono</title>
</head>
  
<body> 

<h3>Henkilöt</h3>

<?php
$henkilot = array(
   "laale" => "Laaksonen Leena;TIPI;29.02.1975",
   "korma" => "Korhonen Maija;TIKO;01.03.1981",
   "nieer" => "Nieminen Erkki;BITE;24.12.1940",
   "virpe" => "Virtanen Pekka;TIP;24.03.1985"
   );
   
ksort($henkilot);
?>

<table border="1" cellpadding="5">
<tr>
<th>Nimi</th>
<th>Toiminto</th>
</tr>

<?php
foreach ($henkilot as $tunnus => $tiedot) {
$tieto = explode(";", $tiedot);
?>
<tr>
<td><?php echo $tieto[0] ?></td>
<td>
  <a href="nayta.php<?php printf('?to=%s&t=%s&n=%s&k=%s&pvm=%s', 'nayta',$tunnus, $tieto[0], $tieto[1], $tieto[2]); ?>">Näytä</a>
  <a href="nayta.php<?php printf('?to=%s&t=%s&n=%s&k=%s&pvm=%s', 'muuta',$tunnus, $tieto[0], $tieto[1], $tieto[2]); ?>">Muuta</a>
  <a href="nayta.php<?php printf('?to=%s&t=%s&n=%s&k=%s&pvm=%s', 'poista',$tunnus, $tieto[0], $tieto[1], $tieto[2]); ?>">Poista</a>
</td>
</tr>
<?php }?>

</table>


</body>
</html>