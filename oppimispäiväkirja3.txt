Mika Kumanto 0702483 - Web-ohjelmointi

Etätehtävä 3
------------

* Olivatko oppimistavoitteet selvät?
Melko lyhyet tehtävät oli ja osa keskeisistä asioista olin jo aikasemmin tehnyt

* Oliko oppimateriaalista hyötyä tehtävien tekemisessä?
En käyttänyt oppimateriaaleja

* Oliko tehtävistä hyötyä oppimisen kannalta?
Eiköhän sitä kaikilla keskeisillä tavoilla oppinut tilaa hallitsemaan, mutta
cookie-esimerkki oli vähän ehkä hyödytön. Siis käytännössä en tie oikein, mitä
hyötyä käyttäjälle tuonlaisesta cookiesta olisi. Enemmän ne tilastot kiinnostaa
kuitenkin sivuston ylläpitäjää ja cookie olisi vaikka parempi tallentaa sivustoa
koskevaa pitempi aikaista tietoa kuten kieliasetuksia tai vastaavia

* Mikä oli helppoa, mikä vaikeaa? Miksi?
En tiedä oliko mikään erityisen vaikeata. Sessioniin olion serialisointi vähän
arveluttavaa ja tehtävä 1 henkilö-esimerkki oli melkoinen purkkaviritelmä.
Periaatteessahan olisi voinut hyödyntää sitä staattista listaa myöhemminkin ja
pelkästään linkissä olisi avaimen arvo. Esim kysely.php?toiminta=muuta&tunnus=laale.
Nyt tarvii kaikki näytettävä tieto url-riville tallentaa ja toki esimerkki vain
on, mutta vähän arveluttava sellainen

* Miten hyvin saavutit oppimistavoitteet?
Kaiken sain tehtyä, että eiköhä ne tullut saavutettua.

* Paljonko aikaa kului tehtävien tekemiseen?
En muista tarkalleen, kun osan tehtävistä olin tehnyt aikasemmin ja viimeistelin
osan vasta nyt myöhemmin. Myös paljon aikaa meni siihen, että kehitin loppuun
toisella koneella ja kehitysympäristön asentaminen ei ole yhtä helppoa kuin
esimerkiksi railsin tai javan rajapintojen kanssa.