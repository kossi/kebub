<?php
//redbean dabase  
// abc
require('rb.php');
include("entities/review.php");
R::setup('sqlite:'.$_SERVER["DOCUMENT_ROOT"].'/kebub.db','keke','lol123');


// http://anantgarg.com/2009/03/13/write-your-own-php-mvc-framework-part-1/
function stripSlashesDeep($value) {
  $value = is_array($value) ? array_map('stripSlashesDeep', $value) : stripslashes($value);
  return $value;
}

if (get_magic_quotes_gpc()) {
  $_GET = stripSlashesDeep($_GET);
  $_POST = stripSlashesDeep($_POST);
  $_COOKIE = stripSlashesDeep($_COOKIE);
}

$sites = array(
  "front" => "<i class=\"icon-home icon-white\"></i> Index",
  "add" => "<i class=\"icon-pencil icon-white\"></i> Lisää ravintola",
  "list" => "<i class=\"icon-list icon-white\"></i> Lista ravintoloista",
  "find" => "<i class=\"icon-search icon-white\"></i> Etsi ravintola",
  "rem" => "<i class=\"icon-trash icon-white\"></i> Poista ravintola",
  "stats" => "<i class=\"icon-time icon-white\"></i> Tilasto"
);

$subsites = array(
  "save" => "Tallenna ravintola",
  "saved" => "Tallennettu ravintola"
);

$allsites = array_merge($sites, $subsites);

if(isset($_GET["l"]) && array_key_exists($_GET["l"], $allsites))
  $site = $_GET['l'];
else 
  $site = "front";

if(isset($_COOKIE[$site]) && !array_key_exists($site, $subsites) && is_numeric($_COOKIE[$site]))
  setcookie($site, $_COOKIE[$site]+1);
elseif(!array_key_exists($site, $subsites))
  setcookie($site, 1, time() + 60*60*24*30);

include("templates/header.php");
include("sites/".$site.".php");
include("templates/footer.php");

?>

